import torch
import torch.nn as nn
import numpy as np




from NNModel import VoiceSexReconizeNN

from utils import *





# 0) Prepare data
if direxists('dataset'):
  os.system('title Загрузка датасета')
  arr_numpys=GetDataset('dataset')

shape=np.load(arr_numpys[1])
shape=torch.from_numpy(shape.astype(np.float32))




n_features=128


model = VoiceSexReconizeNN(n_features)

# 2) Loss and optimizer
num_epochs = 100
learning_rate = 0.01
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)



# 3) Training loop

for epoch in range(num_epochs):
    os.system('title epoch '+str(epoch))
    #for i in range(0,500):
    for i in range(0,len(arr_numpys)-2000):
    #for i in range(0,10):    

        #os.system('title epoch '+str(epoch)+' sample '+str(i))

        X_train=np.load(arr_numpys[i])

        #if arr_numpys[i].find('_1.npy'):
        #    y_train=torch.tensor([100.0])
        #if arr_numpys[i].find('_0.npy'):
        #    y_train=torch.tensor([0.0])


        if arr_numpys[i].find('_1.npy')>1:
            y_train=torch.tensor([0.0])
        if arr_numpys[i].find('_0.npy')>1:
            y_train=torch.tensor([1.0])


        X_train=torch.from_numpy(X_train.astype(np.float32))

        # Forward pass and loss
        y_pred = model(X_train)

        #print(y_train,y_pred)
        loss = criterion(y_pred, y_train)


        # Forward pass and loss
        y_pred = model(X_train)
        loss = criterion(y_pred, y_train)


        #print(loss.item())
        # Backward pass and update


        loss.backward()
        # zero grad before new step
        optimizer.step()
        optimizer.zero_grad()





torch.save(model,'sex_recognize_1_male_0_female.pt')

print('OK')
