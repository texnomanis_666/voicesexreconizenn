The task of this neural network is to determine the male voice on the recording or the female one. The neural network has already been trained and its accuracy is approximately 97%. You can use it in your projects at your own risk.

The following components have been used:
- python 3.x
- librosa
- pytorch
- numpy

Operating principle.

Before definition, the sound is converted into a mel spectrogram. Then it is converted into a format that is understandable by the neural network. After that, the received data is sent to the input

layer of the neural network, and its output layer will be the result. 1 male voice, 2 female voice.

The neural network consists of five fully connected layers. Sigmoid is used as an activation function.

I used **[this training dataset](https://yadi.sk/d/IUUTPJFOfwn_OQ)** for training.

I hope this project is useful to you.
