
import torch.nn as nn
import numpy as np
import torch

from NNModel import VoiceSexReconizeNN

from utils import *

spectr = np.array([])
good=0
bad=0


# 0) Prepare data
if direxists('dataset'):
  os.system('title Загрузка датасета')
  arr_numpys=GetDataset('dataset')

model=torch.load('sex_recognize_1_male_0_female.pt')

for i in range(len(arr_numpys)-2000,len(arr_numpys)):


  spectr=np.load(arr_numpys[i])
  spectr=torch.from_numpy(spectr.astype(np.float32))
  rez=model(spectr)

  if rez<0.5:
      rez=0
  else:
      rez=1


  if arr_numpys[i].find('_1.npy')>1:
    rez2=0
  if arr_numpys[i].find('_0.npy')>1:
    rez2=1


  if rez==rez2:
     good+=1
  else:
     bad+=1

print(f'bad:{bad}   good:{good}' )