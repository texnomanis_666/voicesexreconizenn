
import os
import pathlib
import librosa
import numpy as np
import torch


# получить список всех папок и файлов из папки
def GetDataset(dir):
 rez=[]
 dir=os.path.normpath(dir)
 currentDirectory = pathlib.Path(dir)
 for currentFile in currentDirectory.iterdir():
  rez.append(os.path.normpath(str(currentFile)))
 return rez


# проверить наличие папки
def direxists(file_name):
 rez=0
 if ( (os.path.exists(file_name)==True) and (os.path.isfile(file_name)==False)):
  rez=1
 return rez


def GetSpectr(wav):
    X, sample_rate = librosa.core.load(wav)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
    spectr = np.hstack((spectr, mel))

    return spectr

    #spectr=torch.from_numpy(spectr.astype(np.float32))
    #return spectr