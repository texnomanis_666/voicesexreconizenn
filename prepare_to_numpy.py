
import librosa
import numpy as np
import os
import csv




# сохранить массив в файл
def array_save_to_file(array,txt_file):
 with open(txt_file, 'w',encoding="utf-8") as filehandle:  
  for listitem in array:
   filehandle.write('%s\n' % listitem)

 
# загрузить текстовый файл в массив
def load_txt_to_array(txt_file):
# для кодировки utf-8
 try:
  with open(txt_file,encoding="utf-8") as f:
   array = f.read().splitlines()
   rez=1
 except:
  rez=0
 if (rez==1):
  return array

# для кодировки ansi
 try:
  with open(txt_file) as f:
   array = f.read().splitlines()
   rez=1
 except:
  rez=0
 if (rez==1):
  return array
  
  # тут добавить другие возможные кодировки

# если ничего не получилось то вернуть 0  
 if (rez==0):
  return 0



# проверить наличие папки
def direxists(file_name):
 rez=0
 if ( (os.path.exists(file_name)==True) and (os.path.isfile(file_name)==False)):
  rez=1
 return rez



if direxists('dataset')==0:
    os.mkdir('dataset')


files=load_txt_to_array('train\\targets.tsv')

for i in range(0,len(files)):

  os.system('title Making dataset in NumPy '+str(i)+' / '+str(len(files)))
  data=files[i].split()
  spectr = np.array([])
  j=dict()

  print(data[0],data[1])

  X, sample_rate = librosa.core.load('train\\'+str(data[0])+'.wav')
  mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
  spectr = np.hstack((spectr, mel))

  np.save('dataset\\'+str(data[0])+'__'+str(data[1])+'.npy',spectr)



